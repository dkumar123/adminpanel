const path = require('path')
//const withSass = require('@zeit/next-sass');


module.exports = {
  webpack: (config) => {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack']
    });
    return config;
  },
  future: {
    webpack5: true
  }
};



module.exports = {
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
}