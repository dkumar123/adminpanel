const plugins = ['postcss-import', 'tailwindcss', 'postcss-preset-env', 'autoprefixer'];

module.exports = {
  plugins
};
