import React, { useState } from 'react';
import { useRouter } from 'next/router';

const Instapreps = () => {
  const router = useRouter();
  const [open, setOpen] = useState(false);

  return (
    <div className='p-2'>
      <div className=' m-auto mb-4 sm:w-[69%] w-[100%]'>
        <div className='flex justify-between '>
          <a href='/'>
            <img className=' h-14 sm:h-20' src='/images/instapreps.png' alt='' />
          </a>
          <div className='flex items-center -mr-2 md:hidden'>
            <button
              type='button'
              className='inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-600'
              aria-expanded='false'
              onClick={() => setOpen(true)}
            >
              <svg
                className='w-6 h-6'
                xmlns='http://www.w3.org/2000/svg'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
                aria-hidden='true'
              >
                <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M4 6h16M4 12h16M4 18h16' />
              </svg>
            </button>
          </div>

          <button
            onClick={() => window.open('https://play.google.com/store/apps/details?id=com.instapreps&hl=en_US&gl=US')}
            className=' hidden md:flex self-center text-2xl text-white bg-[#55d7ab] rounded-4xl  p-2 px-10 focus:outline-none hover:shadow-secondary hover:-translate-y-1  transform duration-300'
          >
            Download
          </button>
          <div
            className={`${
              open ? 'block' : 'hidden'
            } z-20 absolute inset-x-0 top-0  transition origin-top transform md:hidden`}
          >
            <div className='overflow-hidden  bg-white rounded-lg ring-black ring-opacity-5'>
              <div className='flex items-center justify-between  p-2'>
                <div>
                  <img className='w-auto h-14 sm:h-20' src='/images/instapreps.png' alt='' />
                </div>
                <div className='-mr-2'>
                  <button
                    type='button'
                    className='inline-flex items-center justify-center p-2 text-gray-400  rounded-md hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-600'
                    onClick={() => setOpen(false)}
                  >
                    <span className='sr-only'>Close menu</span>
                    <svg
                      className='w-6 h-6'
                      xmlns='http://www.w3.org/2000/svg'
                      fill='none'
                      viewBox='0 0 24 24'
                      stroke='currentColor'
                      aria-hidden='true'
                    >
                      <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M6 18L18 6M6 6l12 12' />
                    </svg>
                  </button>
                </div>
              </div>
              <div className={`${open ? 'block' : 'hidden'} pt-5 pb-6 `}>
                <div className='px-2 flex flex-col items-center space-y-1 '>
                  <button
                    onClick={() =>
                      window.open('https://play.google.com/store/apps/details?id=com.instapreps&hl=en_US&gl=US')
                    }
                    className=' block self-center text-2xl text-white bg-[#55d7ab] rounded-4xl  p-2 px-10 focus:outline-none hover:shadow-secondary hover:-translate-y-1  transform duration-300'
                  >
                    Download
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='pt-10 sm:pt-16 lg:pt-8 lg:pb-14 lg:overflow-hidden'>
          <div className='mx-auto max-w-7xl '>
            <div className='lg:grid lg:grid-cols-2 lg:gap-8'>
              <div className='max-w-md px-4 mx-auto sm:max-w-2xl sm:px-6 sm:text-center lg:px-0 lg:text-left lg:flex lg:items-center'>
                <div className='lg:py-24  '>
                  <p className='text-xl text-indigo sm:text-2xl '>World's 1st Failure Predictor App</p>
                  <h1 className='my-4 text-3xl md:text-4xl font-extrabold  sm:mt-5 '>
                    <span className='block  text-primary'>Instapreps Pe Aao, Confidence Badhao</span>
                  </h1>
                  <h2 className='font-bold text-xl text-gray-dark sm:text-2xl'>
                    An Initiative by IIT, NIT & Super30 Alumni
                  </h2>
                  <button
                    onClick={() => router.push('https://play.google.com/store/apps/details?id=com.instapreps')}
                    className='mt-20  items-center self-center  w-full sm:w-[80%] m-auto py-3  text-xl font-medium text-white transition focus:outline-none  bg-[#6554e1] border border-transparent rounded-4xl sfocus:outline-none hover:shadow-secondary hover:-translate-y-1  transform duration-300 hover:bg-gray-700'
                  >
                    Download
                  </button>
                </div>
              </div>
              <div className='mt-12 -mb-16 sm:-mb-48 lg:m-0 lg:relative self-center'>
                <div className='max-w-md px-4 mx-auto sm:max-w-2xl sm:px-6 lg:max-w-none lg:px-0'>
                  {/* Will fade in and out */}
                  <img id='landingImage' className=' ' src='/images/landing-img.png' alt='' />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='  w-[90%] m-auto'>
        <div className='block md:grid grid-cols-2 mt-14 md:mt-12 '>
          <div className='pt-0 md:pt-6 '>
            <div className='max-w-md px-4 mx-auto sm:max-w-2xl sm:px-6 sm:text-center lg:px-0 lg:text-left lg:flex lg:items-center'>
              <div className='mt-2'>
                <h1 className=' text-4xl font-medium  mt-5 '>
                  <span className='block  text-indigo'>Instapreps </span>
                </h1>
                <h2 className='font-semibold text-2xl text-gray-dark '>The Confidence App</h2>
              </div>
            </div>
            <div className='grid grid-cols-4 space-y-6   p-0 sm:p-2 mt-10 '>
              <div className='flex self-center justify-center'>
                <p className='border-2 border-gray-700  px-4  lg:px-8  py-3 text-center rounded-full'>01</p>
              </div>
              <div className='flex justify-center  col-span-3'>
                <p className='text-sm sm:text-xl'>
                  The app that helps you in identifying weaknesses in a subject by answering a series of questions,
                  crafted by IIT faculty.
                </p>
              </div>
              <div className='flex justify-center self-center'>
                <p className='border-2 border-gray-700 px-4  lg:px-8 py-3   text-center rounded-full'>02</p>
              </div>
              <div className='flex justify-center  col-span-3'>
                <p className='text-sm sm:text-xl'>A detailed report of the areas where one needs to buck up.</p>
              </div>
              <div className='flex self-center justify-center'>
                <p className='border-2 border-gray-700 px-4  lg:px-8  py-3 m-3  text-center rounded-full'>03</p>
              </div>
              <div className=' flex justify-center  col-span-3'>
                <p className='text-sm sm:text-xl'>
                  Evaluates confidence across different chapters and tells you how to go about planning, while you wait
                  for the exams.
                </p>
              </div>
            </div>
            <div className=' my-10 p-0 sm:p-5'>
              <div className='block sm:flex justify-between mt-10  text-center'>
                <button
                  onClick={() => router.push('https://apps.apple.com/in/app/instapreps/id1543607970')}
                  className=' items-center inline-flex self-center px-6 py-2  text-lg md:text-xl font-medium text-white transition focus:outline-none bg-[#6554e1] border border-transparent rounded-4xl  hover:shadow-secondary hover:-translate-y-1  transform duration-300 hover:bg-gray-700'
                >
                  <img id='' className='inline-flex h-6 mr-2  self-center' src='/images/appleIcon.svg' alt='' />
                  Download The App
                </button>
                <div className='block md:flex items-center mt-4 sm:mt-0 '>
                  <span className='font-semibold text-2xl sm:text-4xl'>4.6</span>
                  <span className='font-semibold text-xl  sm:text-2xl text-gray-500'>/5</span>
                  <img id='fullStar' className='inline-flex ' src='/images/fullStar.svg' alt='' />
                  <img id='fullStar' className='inline-flex ' src='/images/fullStar.svg' alt='' />
                  <img id='fullStar' className='inline-flex ' src='/images/fullStar.svg' alt='' />
                  <img id='fullStar' className='inline-flex ' src='/images/fullStar.svg' alt='' />
                  <img id='halfStar' className='inline-flex ' src='/images/halfStar.svg' alt='' />
                </div>
              </div>
              <div className='block sm:flex justify-between mt-10 text-center'>
                <button
                  onClick={() => router.push('https://play.google.com/store/apps/details?id=com.instapreps')}
                  className='   items-center self-center px-6 py-2  text-lg md:text-xl font-medium text-white transition  bg-[#55d7ab] border border-transparent focus:outline-none rounded-4xl sfocus:outline-none hover:shadow-secondary hover:-translate-y-1  transform duration-300 hover:bg-gray-700'
                >
                  <img id='fullStar' className='inline-flex h-6 mr-2 self-center' src='/images/googlePlay.svg' alt='' />
                  Download The App
                </button>
                <div className='block md:flex items-center  mt-4 sm:mt-0 '>
                  <span className='font-semibold text-2xl sm:text-4xl'>4.7</span>
                  <span className='font-semibold  text-xl  sm:text-2xl text-gray-500'>/5</span>
                  <img id='fullStar' className='inline-flex ' src='/images/fullStar.svg' alt='' />
                  <img id='fullStar' className='inline-flex ' src='/images/fullStar.svg' alt='' />
                  <img id='fullStar' className='inline-flex ' src='/images/fullStar.svg' alt='' />
                  <img id='fullStar' className='inline-flex ' src='/images/fullStar.svg' alt='' />
                  <img id='halfStar' className='inline-flex ' src='/images/halfStar.svg' alt='' />
                </div>
              </div>
            </div>
          </div>
          <div className='p-3'>
            <img id='assessmentImage' className='w-full h-[90%] ' src='/images/assesmentImg.png' alt='' />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Instapreps;
