import { Disclosure } from '@headlessui/react'
import { ChevronUpIcon } from '@heroicons/react/solid'
import Button from '@material-ui/core/Button';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import IconButton from '@material-ui/core/IconButton';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from "react";
import axios from 'axios';
//import { questionList } from 'src/components/QuestionList';
const Question1 = () => {
  const router = useRouter();
  {/*const handleInput= () => {
router.push('/admin1');
}*/}


  const [currentQ, setCurrentQ] = useState([])
  const [result, setResult] = useState(0)
  // const handleNextQuestion =() =>{
  //   if(currentQ < questionList.length -1){
  //     setCurrentQ(currentQ + 1);
  //   }

  //   //router.push('/adminhm/question2');
  // }

  // const handlePrevQuestion =() =>{
  //   if(currentQ < questionList.length){
  //     setCurrentQ(currentQ - 1);
  //   }
  // }



  useEffect(() => {
    const getGrades = async () => {
      const response = await axios.get('https://cors-anywhere1223.herokuapp.com/https://staging.instapreps.com/api/questions');
      console.log(response.data)
      setCurrentQ({ currentQ: response.data });

    };
    getGrades();
  }, []);


  return (
    <div class="flex ...">

      <div class="w-1/4 ... shadow-xl h-max border-4">

        <div class="py-7 px-20">
          <p class='px-10 py-4 text-xl'>Admin</p>
          <a href='/'><img className='w-30 md:w-15' src='/images/Ranjan.png' alt='' />
          </a>
          <p class='px-0 text-2xl text-blue-700 pr-6'>Ranjan kumar</p>
        </div>

        <div class="flex space-x-4 ... px-4">
          <div> <img className='w-20 ' src='/images/book.png' alt='' />

          </div>
          <div class='pr-2 pb-2 pt-4'>
            <p class='text-black text-xl text-opacity-100 underline ...' ><a href='/Adminhm'>Testlist</a></p>
          </div>
        </div>
        <div class="flex space-x-4 ... px-4 py-4">
          <div> <img className='w-20 ' src='/images/book1.png' alt='' />

          </div>
          <div class='pr-2 pb-2 pt-4'>
            <p class='text-black text-xl text-opacity-100 underline ...'><a href='/adminhm/question'>Question</a></p>
          </div>
        </div>
      </div>
      <div class="w-4/5">
        <div class='py-4 px-24 pr-4 '>
          <div class="relative rounded-lg ... border-2 border-indigo-600 ... h-20 w-4/5 shadow-2xl py-4 ">
            <div class='absolute bottom-0 right-0 h-16 w-16 ... py-4  border-1 shadow-md'>
              <input
                type="file"
                accept="image/*"
                style={{ display: 'none' }}
                id="contained-button-file"
              />
              <label htmlFor="contained-button-file">
                <Button variant="contained" color="white" component="span">
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12" />
                  </svg>
                </Button>
              </label></div>
            <p class='text-5xl md:text-xl pl-8'>Upload question from excel sheet</p>

          </div>

        </div>
        {/* <div class='grid justify-items-center ...pl-8 text-3xl'>or</div> */}

        <div class='pt-8 pl-8 '>
          <div class='px-24'>
            <div class="rounded-lg ... border-2 border-indigo-600 ... w-4/5 shadow-2xl  ">
              <div class='text-2xl pl-8 pt-2 py-4'>Add Questions</div>
              <div class='px-8'>

                <div class="">
                  <div class="">
                    <div class='py-6 flex '>
                      <div class="rounded-lg border-2 border-indigo-600 ... h-24 w-4/5 to shadow-xl relative">
                        {/* {currentQ[result].map((items) => {
                          retrurn(
                            <p class='text-xl' key={items.id}>{items.question_body}

                            </p>
                          );
                        })} */}
                        {/*{currentQ[result].question_body}*/}

                        <div class="absolute bottom-0 right-0 h-8 w-8 mr-8...">
                          <input
                            type="file"
                            accept="image/*"
                            style={{ display: 'none' }}
                            id="contained-button-file"
                          />
                          <label htmlFor="contained-button-file">
                            <Button variant="" color="white" component="span" class='bg-white '>
                              <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12" />
                              </svg>
                            </Button>
                          </label>

                        </div>

                      </div>
                    </div>
                    <div class='py-4'>
                      <button class="bg-white hover:bg-white text-blue-800 text-sm py-2  rounded-full w-4/5 w-4 border-2 shadow-xl">
                        <p class='text-xl'>{/*{questionList[currentQ].option_one}*/}</p>
                      </button>
                    </div>
                    <div class='py-4'>
                      <button class="bg-green hover:bg-white text-blue-800 text-sm py-2  rounded-full w-4/5 w-4 border-2 shadow-xl">
                        <p class='text-xl '> {/*{questionList[currentQ].option_two}*/}</p>
                      </button>
                    </div>
                    <div class='py-4'>
                      <button class="bg-white hover:bg-white text-blue-800 text-sm py-2  rounded-full w-4/5 w-4 border-2 shadow-xl">
                        <p class='text-xl'>{/*{questionList[currentQ].option_three}*/}</p>
                      </button>
                    </div>
                    <div class='py-4'>
                      <button class="bg-white hover:bg-white text-blue-800 text-sm py-2 rounded-full w-4/5 w-4 border-2 shadow-xl">
                        <p class='text-xl '>{/*{questionList[currentQ].option_four}*/}</p>
                      </button>
                    </div>
                    <div class='py-4'>
                      <button class="bg-grey- hover:bg-white text-blue-800 text-sm py-2 bg-purple-100 rounded-full w-4/5 w-4 border-2 shadow-xl">
                        Please select the correct option
                      </button>
                    </div>
                    <div class='pl-2 pt-4'>
                      <Disclosure>
                        {({ open }) => (
                          <>
                            <Disclosure.Button className="flex justify-between w-40 px-4 py-2 text-sm font-medium text-left text-purple-900 bg-purple-100 rounded-lg hover:bg-purple-200 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                              <span>views solution</span>
                              <ChevronUpIcon
                                className={`${open ? 'transform rotate-180' : ''
                                  } w-5 h-5 text-purple-500`}
                              />
                            </Disclosure.Button>
                            <Disclosure.Panel className="px-4 pt-4 pb-2 text-2xl text-gray-500 border-2 ">
                              <div class=''>If the displacement of a body is proportion to the square of time, then the body is moving with.
                                <input accept="image/*" id="icon-button-file"
                                  type="file" style={{ display: 'none' }} />
                                <label htmlFor="icon-button-file">
                                  <IconButton color="primary" aria-label="upload picture"
                                    component="span">
                                    <PhotoCamera />
                                  </IconButton>
                                </label></div>
                            </Disclosure.Panel>
                          </>
                        )}
                      </Disclosure>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <button type='submit' class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full h-16 w-1/5 text-2xl  colors.indigo shadow-2xl dee3 ">Next</button>

            {/* <div className=' pt-2'>
            <button type='submit'  class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full h-16 w-1/5 text-2xl  colors.indigo shadow-2xl dee3 ">previous</button>
            </div>
               */}



          </div>
        </div>
      </div>


    </div>


  )
}

export default Question1;
