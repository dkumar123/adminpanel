import React from 'react'
import { Disclosure } from '@headlessui/react'
import { ChevronUpIcon } from '@heroicons/react/solid'
import { useRouter } from 'next/router';
const Admin4 = () => {
    const router = useRouter();
    return (
        <div class="flex ...">

            <div class="w-1/4 ... shadow-xl h-max border-2">

                <div class="py-7 px-20">
                    <p class='px-10 py-4 text-xl'>Admin</p>
                    <a href='/'><img className='w-30' src='/images/Ranjan.png' alt='' />
                    </a>
                    <p class='px-0 text-3xl text-blue-700 pr-6 '>Ranjan kumar</p>
                </div>

                <div class="flex space-x-4 ... px-4">
                    <div> <img className='w-20 ' src='/images/book.png' alt='' />

                    </div>
                    <div class='pr-2 pb-2 pt-4'>
                        <p class='text-black text-xl text-opacity-100 underline ...'><a href='/adminhm'>Testlist</a></p>
                    </div>
                </div>
                <div class="flex space-x-4 ... px-4 py-4">
                    <div> <img className='w-20 ' src='/images/book1.png' alt='' />

                    </div>
                    <div class='pr-2 pb-2 pt-4'>
                        <p class='text-black text-xl text-opacity-100 underline ...'><a href='/adminhm/question'>Question</a></p>
                    </div>
                </div>
            </div>
            <div class="w-4/5">
                <div class='py-4 px-24 pr-4 '>
                    {/*<div class="rounded-lg ... border-2 border-indigo-600 ... h-20 w-4/5 shadow-2xl grid justify-items-center ... ... py-4 text-2xl">Upload question from Excel sheet
          </div>*/}
                    <div class='text-2xl'>Review your question</div>

                </div>

                <div class='pt-8 pl-8 '>
                    <div class='px-24 h-screen'>
                        <div class="rounded-lg ... border-2 border-indigo-600 ... w-full shadow-2xl ">
                            <div class='text-2xl pl-8 pt-2 py-4 h-[70%]'>Add Questions</div>
                            <div class='px-8'>

                                <div class="">
                                    <div class="px-8">
                                        <div class='py-6 '>
                                            <div class="rounded-lg border-2 border-indigo-600 ... h-24 w-4/5 shadow-xl overflow-hidden">
                                                <p class='text-2xl'>Q1.If the displacement of a body is proportion to the square of time, then the body is moving with..</p>
                                            </div>
                                        </div>
                                        <div class='py-4'>
                                            <button class="bg-white hover:bg-white text-blue-800 text-sm py-2  rounded-full w-4/5 w-4 border-2 shadow-xl">
                                                1.Uniform acceleration
                                            </button>
                                        </div>
                                        <div class='py-4'>
                                            <button class="bg-green hover:bg-white text-blue-800 text-sm py-2  rounded-full w-4/5 w-4 border-2 shadow-xl">
                                                2.Increasing acceleration
                                            </button>
                                        </div>
                                        <div class='py-4'>
                                            <button class="bg-white hover:bg-white text-blue-800 text-sm py-2  rounded-full w-4/5 w-4 border-2 shadow-xl">
                                                3.Decreasing acceleration
                                            </button>
                                        </div>
                                        <div class='py-4'>
                                            <button class="bg-white hover:bg-white text-blue-800 text-sm py-2 rounded-full w-4/5 w-4 border-2 shadow-xl">
                                                4.Uniform Velocity
                                            </button>
                                        </div>

                                        <div class='pl-2 pt-4'>
                                            <Disclosure>
                                                {({ open }) => (
                                                    <>
                                                        <Disclosure.Button className="flex justify-between w-40 px-4 py-2 text-sm font-medium text-left text-purple-900 bg-purple-100 rounded-lg hover:bg-purple-200 focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                                                            <span>views solution</span>
                                                            <ChevronUpIcon
                                                                className={`${open ? 'transform rotate-180' : ''
                                                                    } w-5 h-5 text-purple-500`}
                                                            />
                                                        </Disclosure.Button>
                                                        <Disclosure.Panel className="px-4 pt-4 pb-2 text-2xl text-gray-500 border-2">
                                                            If the displacement of a body is proportion to the square of time, then the body is moving with.
                                                        </Disclosure.Panel>
                                                    </>
                                                )}
                                            </Disclosure>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=' pr-4 py-16 grid justify-items-center ...'><button type='submit' onClick={()=>router.push('/adminhm/question3')} class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full h-20 w-2/5 text-2xl  colors.indigo shadow-2xl">Submit</button></div>


                    </div>
                </div>
            </div>


        </div>


    )
}

export default Admin4

