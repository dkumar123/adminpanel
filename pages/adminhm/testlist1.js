import React,{useState} from 'react'
import router, { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import Axios from 'axios';
//import {MapRegisterDetails} from '.src/components/adminList/constants'
import { yupResolver } from '@hookform/resolvers/yup';
//import { validationSchemaAdminFirstDetails } from '.src/components/adminList/validation';

import * as yup from 'yup';
import FirstStep from 'src/components/adminList/first_Step';
import SecondStep from 'src/components/adminList/second_step';
import { mapRegisterDetails } from 'src/components/adminList/constants';

 const AdminPg = () => {
    
    const ActionType = {
        Details: 'details',
        Option: 'options'
      };
      const [activeForm, setActiveForm] = useState(ActionType.Details);
      const [details, setDetails] = useState(null);


const onSuccessDetails= async(values)=>{
    setDetails(values);
    setActiveForm(ActionType.Option);
}

const onSuccessConfirmOption= async (values) =>{
    const registerPostData = mapRegisterDetails(details,values);
     await Axios.post(`https://cors-anywhere1223.herokuapp.com/https://staging.instapreps.com/api/testlist/create`, registerPostData);
    
     router.push('/adminhm/testlist3')
}

    return (

        <div className="flex ... gap-4">

            <div className="w-1/4 ... shadow-xl h-screen border-2">

                <div className="py-7 px-20">
                    <p className='px-10 py-4 text-xl'>Admin</p>
                    <a href='/'><img className='w-30' src='/images/Ranjan.png' alt='' />
                    </a>
                    <p className='px-0 text-2xl text-blue-700 pr-6 '>Ranjan kumar</p>
                </div>

                <div className="flex space-x-4 ... px-4">
                    <div> <img className='w-20 ' src='/images/book.png' alt='' />

                    </div>
                    <div className='pr-2 pb-2 pt-4'>
                        <p className='text-black text-xl text-opacity-100 underline ...' ><a href='/adminhm'>Testlist</a></p>
                    </div>
                </div>
                <div className="flex space-x-4 ... px-4 py-4">
                    <div> <img className='w-20 ' src='/images/book1.png' alt='' />

                    </div>
                    <div className='pr-2 pb-2 pt-4'>
                        <p className='text-black text-xl text-opacity-100 underline ...' href='/'>Question</p>
                    </div>
                </div>
            </div>


            <div className="w-full flex justify-center items-center">
            {activeForm === ActionType.Details && (
                            <FirstStep onSuccess={onSuccessDetails}/>
                          )}
            {activeForm === ActionType.Option && (
                            <SecondStep onSuccess={onSuccessConfirmOption}/>
                          )}
            </div>


        </div>









    )
}

export default AdminPg;
