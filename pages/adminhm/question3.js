import React from 'react'

const Admin = () => {
    return (
        <div class='overflow-hidden'>
            <div class='container flex pt-0 sm:flex-wrap gap-4'>
                <div class='w-1/5 h-screen border-2 shadow-xl overflow-hidden'>
                    <div class='flex justify-items-center ... pt-4 px-4'>
                        <div class='justify-items-end px-4'>
                            <p class='px-8 text-xl'>Admin</p>
                            <img class='w-32 h-32' src='/images/Ranjan.png' alt='image' />
                            <p class='text-2xl'>Ranjan kumar</p>
                        </div>
                    </div>
                    <div class='flex  gap-4 py-8'>
                        <div class=''>
                            <img class='h-24 w-24' src='/images/book.png' alt='image' />

                        </div>
                        <div class='underline ... text-xl pt-4'><a href='/adminhm'>Testlist</a></div>
                    </div>
                    <div class='flex  gap-4 py-8'>
                        <div class=''>
                            <img class='h-24 w-24 ' src='/images/book1.png' alt='image' />

                        </div>
                        <div class='underline ...pt-8 text-xl'><a href='/adminhm/question'>Question</a></div>
                    </div>
                </div>


                <div class='w-[70%] h-screen  '>
                    <div class='pt-4 px-4'>
                        <p class='text-2xl'>Question for Velocity</p>
                    </div>
                    <div class='grid justify-items-center ... overflow-hidden gap-8 pt-8'>
                        <div class='w-4/5 h-16 border-2 relative rounded-md shadow-xl'>
                            <div class='absolute inset-y-0 left-0 h-18  w-4 bg-blue-500'></div>
                            <p class='pl-4'>1.If the displacement of an object is proportional to square of time, ...</p>
                            <div class='absolute top-0 right-0 pt-4'>hjhjj</div>

                        </div>
                        <div class='w-4/5 h-16 border-2 relative rounded-md bg-purple-100 shadow-xl'>
                            <div class='absolute inset-y-0 left-0 h-18  w-4 bg-blue-500'></div>
                            <p class='pl-4'>2. When a car driver travelling at a speed of 10 m/s</p>
                            <div class='absolute top-0 right-0 pt-4'>hjhjj</div>

                        </div>
                        <div class='w-4/5 h-16 border-2 relative bg-purple-100 rounded-md shadow-xl'>
                            <div class='absolute inset-y-0 left-0 h-18  w-4 bg-blue-500'></div>
                            <p class='pl-4'>3. When a car driver travelling at a speed of 10 m/s</p>
                            <div class='absolute top-0 right-0 pt-4'>hjhjj</div>

                        </div>
                        <div class='w-4/5 h-16 border-2 relative bg-purple-100 rounded-md shadow-xl'>
                            <div class='absolute inset-y-0 left-0 h-18  w-4 bg-blue-500'></div>
                            <p class='pl-4 text-md'>4. When a car driver travelling at a speed of 10 m/s</p>
                            <div class='absolute top-0 right-0 pt-4'>hjhjj</div>

                        </div>
                        <div class='w-4/5 h-16 border-2 relative rounded-md shadow-xl'>
                            <div class='absolute inset-y-0 left-0 h-18  w-4 bg-blue-500'></div>
                            <p class='pl-4 '>5. If the displacement of an object is proportional to square of time, ...</p>
                            <div class='absolute top-0 right-0 pt-4'>hjhjj</div>

                        </div>
                        <div class='grid justify-items-center ... py-24 px-2 '><button type='submit' class="bg-blue-500  text-white font-bold rounded-full h-20 w-45 text-2xl">Add more question</button></div>



                    </div>

                </div>
            </div>

        </div>
    )
}

export default Admin;
