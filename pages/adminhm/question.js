import React from 'react'
import { useState } from 'react'
import { Switch } from '@headlessui/react'
import { useRouter } from 'next/router';

const  Question = () => {

    const [enabled, setEnabled] = useState(false)


    const router = useRouter();
{/*const handleInput= () => {
router.push('/admin1');
}*/}


    return (
        <div class="flex ...">

            <div class="w-1/4 ... shadow-xl h-max border-4 ">

                <div class="py-7 px-20">
                    <p class='px-10 py-4 text-xl'>Admin</p>
                    <a href='/'><img className='w-30' src='/images/Ranjan.png' alt='' />
                    </a>
                    <p class='px-0 text-xl sm:text-md text-blue-700 pr-6 '>Ranjan kumar</p>
                </div>

                <div class="flex space-x-4 ... px-4">
                    <div> <img className='w-20 ' src='/images/book.png' alt='' />

                    </div>
                    <div class='pr-2 pb-2 pt-4'>
                        <p class='text-black text-xl text-opacity-100 underline ...'><a href='/adminhm'>Testlist</a></p>
                    </div>
                </div>
                <div class="flex space-x-4 ... px-4 py-4">
                    <div> <img className='w-20 ' src='/images/book1.png' alt='' />

                    </div>
                    <div class='pr-2 pb-2 pt-4'>
                        <p class='text-black text-xl text-opacity-100 underline ...' href='/'>Question</p>
                    </div>
                </div>
            </div>
            <div class="w-4/5 ">
                <div class='pl-20 pt-8'>
                    <div class='text-3xl px-4'> Welcome</div>
                    <div class='text-4xl px-4'> Powai School</div>
                </div>


                <div class='pl-32 py-16 px-16'>
                    <div class='shadow-xl px-8 py-8 border-2 w-4/5 overflow-hidden'>
                        <div class="flex space-x-6 ... py-4">
                            <div class='text-3xl '>Your Test List</div>
                            <div class='px-40 pr-16'>

                                <input class="shadow appearance-none rounded-md w-full py-4 px-4 text-gray-700 leading-tight focus:outline-none focus:shadow-outline place-items-end" id="username" type="text" placeholder="search more question" />
                            </div>
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-10 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 4a1 1 0 011-1h16a1 1 0 011 1v2.586a1 1 0 01-.293.707l-6.414 6.414a1 1 0 00-.293.707V17l-4 4v-6.586a1 1 0 00-.293-.707L3.293 7.293A1 1 0 013 6.586V4z" />
                            </svg>
                        </div>
                        <div class='py-4 '>
                            <div class="rounded-lg ... bg-blue-500 w-4/5 overflow-hidden dee1">
                                <div class="flex justify-between ...p">
                                    <div class="order-last">
                                        <div class='text-2xl text-white px-2 py-2'>Physics</div>
                                        <div class='text-xl text-white px-2 '>Speed</div>
                                        <div class="flex space-x-2 ... py-2">
                                            <div class='px-2 text-white'>0 Question</div>
                                            <div class='text-white'>0 Minutes</div>
                                        </div>
                                    </div>

                                    <div class='py-2 pr-4 order-last'><button class="bg-white hover:bg-white text-blue-800 text-sm py-2 px-4 rounded-full">
                                        Add question
                                    </button>
                                    <div class='py-4 px-8 pr-8'>
                                        <Switch
                                            checked={enabled}
                                            onChange={setEnabled}
                                            className={`${enabled ? 'bg-blue-600' : 'bg-gray-200'
                                                } relative inline-flex items-center h-6 rounded-full w-11`}
                                        >
                                            <span className="sr-only">pumlished</span>
                                            <span
                                                className={`${enabled ? 'translate-x-6' : 'translate-x-1'
                                                    } inline-block w-4 h-4 transform bg-white rounded-full`}
                                            />
                                        </Switch>
                                        </div>
                                        

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='py-2 '>
                            <div class="rounded-lg ... bg-blue-500 w-4/5 ">
                                <div class="flex justify-between ...p">
                                    <div class="order-last">
                                        <div class='text-2xl sm:text-xl text-white px-2 py-2'>Physics </div>
                                        <div class='text-xl text-white px-2 '>Velocity</div>
                                        <div class="flex space-x-2 ... py-2">
                                            <div class='px-2 text-white'>0 Question</div>
                                            <div class='text-white'>0 Minutes</div>
                                        </div>
                                    </div>

                                    <div class='py-2 pr-4 order-last'><button class="bg-white hover:bg-white text-blue-800 text-sm py-2 px-4 rounded-full">
                                        Add question
                                    </button>
                                    <div class='py-4 px-8 pr-8'>
                                        <Switch
                                            checked={enabled}
                                            onChange={setEnabled}
                                            className={`${enabled ? 'bg-blue-600' : 'bg-gray-200'
                                                } relative inline-flex items-center h-6 rounded-full w-11`}
                                        >
                                            <span className="sr-only">pumlished</span>
                                            <span
                                                className={`${enabled ? 'translate-x-6' : 'translate-x-1'
                                                    } inline-block w-4 h-4 transform bg-white rounded-full`}
                                            />
                                        </Switch>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='py-2'>
                            <div class="rounded-lg ... bg-blue-500 w-4/5 dee2">
                                <div class="flex justify-between ...p">
                                    <div class="order-last">
                                        <div class='text-2xl text-white px-2 py-2'>Physics</div>
                                        <div class='text-xl text-white px-2 '>Velocity</div>
                                        <div class="flex space-x-2 ... py-2">
                                            <div class='px-2 text-white'>0 Question</div>
                                            <div class='text-white'>0 Minutes</div>
                                        </div>
                                    </div>

                                    <div class='py-2 pr-4 order-last'><button class="bg-white hover:bg-white text-blue-800 text-sm py-2 px-4 rounded-full">
                                        Add question
                                    </button>
                                    
                                    <div class='py-4 px-8 pr-8'>
                                        <Switch
                                            checked={enabled}
                                            onChange={setEnabled}
                                            className={`${enabled ? 'bg-blue-600' : 'bg-gray-200'
                                                } relative inline-flex items-center h-6 rounded-full w-11`}
                                        >
                                            <span className="sr-only">pumlished</span>
                                            <span
                                                className={`${enabled ? 'translate-x-6' : 'translate-x-1'
                                                    } inline-block w-4 h-4 transform bg-white rounded-full`}
                                            />
                                        </Switch>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class='py-2'>
                            <div class="rounded-lg ... bg-blue-500 w-4/5">
                                <div class="flex justify-between ...p">
                                    <div class="order-last">
                                        <div class='text-2xl text-white px-2 py-2'>Physics</div>
                                        <div class='text-xl text-white px-2 '>Speed</div>
                                        <div class="flex space-x-2 ... py-2">
                                            <div class='px-2 text-white'>0 Question</div>
                                            <div class='text-white'>0 Minutes</div>
                                        </div>
                                    </div>

                                    <div class='py-2 pr-4 order-last'><button class="bg-white hover:bg-white text-blue-800 text-sm py-2 px-4 rounded-full">
                                        Add question
                                    </button>
                                    <div class='py-4 px-8 pr-8'>
                                        <Switch
                                            checked={enabled}
                                            onChange={setEnabled}
                                            className={`${enabled ? 'bg-blue-600' : 'bg-gray-200'
                                                } relative inline-flex items-center h-6 rounded-full w-11`}
                                        >
                                            <span className="sr-only">pumlished</span>
                                            <span
                                                className={`${enabled ? 'translate-x-6' : 'translate-x-1'
                                                    } inline-block w-4 h-4 transform bg-white rounded-full`}
                                            />
                                        </Switch>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>


                    </div>
                </div>



                <div class='grid justify-items-center ... py-8 px-2 pr-20'><button type='submit' onClick={()=>router.push('/adminhm/question1')} class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full h-20 w-72 text-3xl colors.indigo shadow-2xl dee3">ADD MORE TEST</button></div>
            </div>

        </div>


    )
}

export default Question;
