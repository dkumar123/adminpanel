/* eslint-disable react/no-danger */
import React from 'react';
import Document, { Head, Main, NextScript, Html } from 'next/document';

export default class MyDocument extends Document {

  render() {
    return (
      <Html lang='en'>
        <Head>
          <meta charSet='utf-8' key='charset' />
          <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
          <meta name='keywords' content='Online classes' />
          <meta name="description" content="Live Online Courses, Online and Offline Jee crash course for 2020 by IIT, NIT, Super30 Alumni for 4th-12th, K12, CBSE NCERT, ICSE, IIT-JEE &amp; NEET | Online Learning for International Competitive Exams."/>
          <meta name='theme-color' content='#fff' />
          <link rel='icon' type='image/png' sizes='32x32' href='/images/favicon.png' />
          <link rel='manifest' href='/manifest.json' />
          <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css' />
          <script async src='https://www.googletagmanager.com/gtag/js?id=G-0SPFWGB31L' />

          {/* <script type='text/javascript' id='hs-script-loader' async defer src='//js.hs-scripts.com/14565324.js' /> */}
          <script async src='https://www.googletagmanager.com/gtag/js?id=AW-690415539' />
          <script
            type='text/javascript'
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: `!window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
            
              gtag('config', 'AW-690415539');`
            }}
          />
          <script
            type='text/javascript'
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: `gtag('event', 'conversion', {'send_to': 'AW-690415539/wAjDCNmnj84CELPPm8kC'});`
            }}
          />

          <script
            type='text/javascript'
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: `!function(f,b,e,v,n,t,s)
              {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
              n.callMethod.apply(n,arguments):n.queue.push(arguments)};
              if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
              n.queue=[];t=b.createElement(e);t.async=!0;
              t.src=v;s=b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t,s)}(window, document,'script',
              'https://connect.facebook.net/en_US/fbevents.js');
              fbq('init', '308941220973794');
              fbq('track', 'PageView');`
            }}
          />
          <noscript
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: `<img height="1" width="1" style="display:none"
              src="https://www.facebook.com/tr?id=308941220973794&ev=PageView&noscript=1"
              />`
            }}
          />

          <script
            type='text/javascript'
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: `!function(f,b,e,v,n,t,s)
              {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
              n.callMethod.apply(n,arguments):n.queue.push(arguments)};
              if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
              n.queue=[];t=b.createElement(e);t.async=!0;
              t.src=v;s=b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t,s)}(window, document,'script',
              'https://connect.facebook.net/en_US/fbevents.js');
              fbq('init', '195319915913603');
              fbq('track', 'PageView');`
            }}
          />
          <noscript
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: `<img height="1" width="1" style="display:none"
              src="https://www.facebook.com/tr?id=195319915913603&ev=PageView&noscript=1"
              />`
            }}
          />
        </Head>
        <body>
          <noscript
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NNNTV8X"
                        height="0" width="0" style="display:none;visibility:hidden">`
            }}
          />
          <Main />
          <div id='portal' />
          <NextScript />
        </body>
      </Html>
    );
  }
}
