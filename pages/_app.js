import React, { Fragment } from 'react';
import Head from 'next/head';
import Router from 'next/router';
import NProgress from 'nprogress';
import apiClient from 'src/apiClient';
import { Provider } from 'components/auth/provider';
import 'styles/tailwind.css';
import 'nprogress/nprogress.css';
import'styles/admin.css';

import'styles/olympiad.css';
import 'styles/vendors.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import styles from "../styles/sat.module.scss";
import 'styles/sataccordion.css';
import 'styles/satlanding.css';



Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const client = apiClient();

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title> InstaPreps : World's 1st Failure Predictor App </title>
        <meta
          name='viewport'
          content='width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no'
          key='viewport'
        />
        
        <link rel="icon" href="/images/instapreps.png"></link>
      </Head>
      <Provider client={client}>
        <Component {...pageProps} />
      </Provider>
    </>
  );
}

export default MyApp;
