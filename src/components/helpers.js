export const getApiBaseUrl = () => {
  return process.env.NEXT_PUBLIC_BASE_API_URL;
};

export const getApiDemoBaseUrl = () => {
  return process.env.NEXT_PUBLIC_BASE_DEMO_API_URL;
};
