import * as yup from 'yup';

//const phoneErrorMessage = 'Please enter valid mobile number';

export const validationSchemaAdminFirstDetails = yup
  .object()
  .shape({
    

    testName: yup.string().required('Please enter the test name'),
    numberOfQuestion: yup.string().required('Please enter the number of question'),
    duration: yup.string().required('Please enter the duration'),
    
  })
  .defined();

export const validationSchemaAdminSecondDetails = yup
  .object()
  .shape({
    board: yup.string().required('Please select valid board'),
    class: yup.string().required('Please select valid board'),
    subject: yup.string().required('Please select valid subject'),
    topic: yup.string().required('Please enter valid topic'),
    
  })
  .defined();
