import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useRouter } from 'next/router';
import { validationSchemaAdminSecondDetails } from './validation';


const SecondStep = ({ onSuccess }) => {
    const [loadingGrades, setLoadingGrades] = useState(true);
    const [loadingBoards, setLoadingBoards] = useState(true);
    const [loadingSubjects, setLoadingSubjects] = useState(true);
    const [grades, setGrades] = useState([]);
    const [boards, setBoards] = useState([]);
    const [subjects, setSubjects] = useState([]);

    useEffect(() => {
        const getGrades = async () => {
            const response = await axios.get(`https://apistaging.7classes.com/api/v1/grades`);
            setGrades(response.data.data);

        };
        getGrades();
    }, []);

    useEffect(() => {
        const getBoards = async () => {
            const response = await axios.get(`https://apistaging.7classes.com/api/v1/boards`);
            setBoards(response.data.data);

        };
        getBoards();
    }, []);

    useEffect(() => {
        const getSubjects = async () => {
            const response = await axios.get(`https://apistaging.7classes.com/api/v1/subjects`);
            setSubjects(response.data.data);

        };
        getSubjects();
    }, []);



    const [topics, setTopics] = useState([])



    const handleBoardChange = async (e) => {
        const response = await axios.get(`https://cors-anywhere1223.herokuapp.com/https://staging.instapreps.com/api/subject/${e.target.value}/topics`);
        console.log(response.data)
        setTopics(response.data);
    };

    const {
        handleSubmit,

        register,
        control,
        formState: { isSubmitting, errors },

    } = useForm({
        defaultValues: {
            class: '',
            board: '',
            topic: '',
            subject: ''
        },
        resolver: yupResolver(validationSchemaAdminSecondDetails)
    });

    const onFormSubmit = async (values) => {
        onSuccess(values);
    };
    return (

        <form onSubmit={handleSubmit(onFormSubmit)}>
            <div className='px-16 pr-6 text-xl '>Test Details</div>
            <div className='relative h-24 w-72 overflow-hidden flex px-8 '>
            
                <div className='absolute left-0 top-0 h-20 w-20 ... border-2 rounded-full shadow-xl bg-blue-500 '><p class='pt-8 pl-8 text-xl'>1</p></div>
                <div className='absolute top-0 right-0 h-20 w-20 ...border-2 rounded-full shadow-xl bg-blue-500'><p class='pt-8 pl-8 text-xl'>2</p></div>

            </div>
            <div className='mt-6'>
                <div className='grid grid-cols-3 sm:grid-cols-12 gap-y-6 md:gap-y-8 gap-x-4'>
                    <div className='col-span-3 sm:col-span-6 '>
                        <label htmlFor='board' className=''>
                            Boards
                        </label>
                        <select
                            id='board'
                            name='board'
                            className='block w-full h-12 px-3 py-2 placeholder-gray-400 border border-blue-400 rounded-md shadow-sm form-select  focus:outline-none focus:border-gray-200 focus:ring-0 sm:text-sm'
                            placeholder={loadingBoards ? 'loading....' : 'Boards'}
                            {...register('board')}
                        >
                            <option value='' disabled className='disabled:text-opacity-50'>
                                Boards
                            </option>
                            {boards.map((board) => (
                                <option key={board.id} value={board.id}>
                                    {board.name}
                                </option>
                            ))}
                        </select>
                        {errors.board && <div className='input-error'>{errors.board.message}</div>}
                    </div>
                    <div className='col-span-3 sm:col-span-6 pt-6'>
                        <label htmlFor='class' className=''>
                            Class
                        </label>
                        <select
                            id='class'
                            name='class'
                            className='block w-full h-12 px-3 py-2 placeholder-gray-400 border border-blue-400 rounded-md shadow-sm form-select  focus:outline-none focus:border-gray-200 focus:ring-0 sm:text-sm'
                            {...register('class')}
                            placeholder={loadingGrades ? 'loading....' : 'Class'}
                        >
                            <option value='' disabled>
                                Class
                            </option>
                            {grades.map((grade) => (
                                <option key={grade.id} value={grade.id}>
                                    {grade.name}
                                </option>
                            ))}
                        </select>
                        {errors.class && <div className='input-error'>{errors.class.message}</div>}
                    </div>
                    <div className='col-span-3 sm:col-span-12 pt-6'>
                        <label htmlFor='subject' className=''>
                            Subject
                        </label>
                        <select
                            id='subject'
                            name='subject'
                            className='block w-full h-12 px-3 py-2 placeholder-gray-400 border border border-blue-400 rounded-md shadow-sm form-select  focus:outline-none focus:border-gray-200 focus:ring-0 sm:text-sm'
                            {...register('subject')}
                            placeholder={loadingSubjects ? 'loading....' : 'Subject'} onChange={handleBoardChange}
                        >
                            <option value='' disabled>
                                Subject
                            </option>
                            {subjects.map((subject) => (
                                <option key={subject.id} value={subject.id}>
                                    {subject.name}
                                </option>
                            ))}
                        </select>
                        {errors.subject && <div className='input-error'>{errors.subject.message}</div>}
                    </div>
                    <div className='col-span-3 sm:col-span-6 pt-6'>
                        <label htmlFor='topic' className=''>
                            Topic
                        </label>
                        <select
                            id='topic'
                            name='topic'
                            className='block w-full h-12 px-3 py-2 placeholder-gray-400 border border-blue-400 rounded-md shadow-sm form-select  focus:outline-none focus:border-gray-200 focus:ring-0 sm:text-sm'
                            placeholder='Topic'
                            {...register('topic')}
                        >
                            <option value='' disabled>
                                Topics
                            </option>
                            {topics.map((topic) => (

                                <option key={topic.id} value={topic.id} >{topic.name}</option>

                            ))}
                        </select>
                        {errors.topic && <div className='input-error'>{errors.topic.message}</div>}
                    </div>

                </div>
            </div>
            <div className='pt-8'>
            <button
                type='submit'
                className='flex justify-center w-full px-4 py-3 text-sm font-medium text-white border border-transparent rounded-md shadow-sm bg-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-200'
                disabled={isSubmitting}
            >
                Next
                {isSubmitting}
            </button>
            </div>
        </form>
    )
}

SecondStep.propTypes = {
    onSuccess: PropTypes.func.isRequired,
}
export default SecondStep
