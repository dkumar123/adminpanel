import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useRouter } from 'next/router';
import { validationSchemaAdminFirstDetails } from './validation';

const FirstStep = ({ onSuccess }) => {


    const {
        handleSubmit,

        register,
        control,
        formState: { isSubmitting, errors },

    } = useForm({
        defaultValues: {
            testName: '',
            numberOfQuestion: '',
            duration: ''
        },
        resolver: yupResolver(validationSchemaAdminFirstDetails)
    });

    const onFormSubmit = async (values) => {
        onSuccess(values);
    };

    return (
        <form onSubmit={handleSubmit(onFormSubmit)}>
            <div className=''>

            <div className='px-16 pr-6 text-xl '>Test Details</div>
            <div className='relative h-24 w-72 overflow-hidden flex px-8 '>
            
                <div className='absolute left-0 top-0 h-20 w-20 ... border-2 rounded-full shadow-xl bg-blue-500 '><p class='pt-8 pl-8 text-xl'>1</p></div>
                <div className='absolute top-0 right-0 h-20 w-20 ...border-2 rounded-full shadow-xl bg-blue-500'><p class='pt-8 pl-8 text-xl'>2</p></div>

            </div>
            <div className='mt-6'>
                <div className='grid grid-cols-12 sm:grid-cols-12 gap-y-6 md:gap-y-8 gap-y-8'>
                    <div className='col-span-3 sm:col-span-16 pb-8'>
                        <label htmlFor='test-Name' className=''>
                            Test Name
                        </label>
                        <input
                            type='text'
                            name='testName'
                            id='testName'
                            autoComplete='testName'
                            placeholder='Test Name'
                            className='block w-full h-10 px-3 py-2 placeholder-gray-400 border-2 border-blue-500 rounded-md shadow-sm  focus:outline-none focus:border-gray-200 focus:ring-0 sm:text-sm'
                            {...register('testName')}
                        />
                        {errors.testName && errors.testName.message && <div className='el-input-error'>{errors.testName.message}</div>}
                    </div>
                    <div className='col-span-3 sm:col-span-12 pt-8'>
                        <label htmlFor='name' className=''>
                            Number Of Question
                        </label>
                        <input
                            type='text'
                            name='numberOfQuestion'
                            id='numberOfQuestion'
                            autoComplete='numberOfQuestion'
                            placeholder='No of Question'
                            className='block w-full h-10 px-3 py-2 placeholder-gray-400 border-2 border-blue-500 rounded-md shadow-sm  focus:outline-none focus:border-gray-200 focus:ring-0 sm:text-sm'
                            {...register('numberOfQuestion')}
                        />
                        {errors.numberOfQuestion && errors.numberOfQuestion.message && <div className='el-input-error'>{errors.numberOfQuestion.message}</div>}
                    </div><div className='col-span-3 sm:col-span-12 pt-8'>
                        <label htmlFor='name' className=''>
                            Duration
                        </label>
                        <input
                            type='text'
                            name='duration'
                            id='duration'
                            autoComplete='duration'
                            placeholder='Duration'
                            className='block w-[100%] h-10  px-3 py-2 placeholder-gray-400 border-2 border-blue-500 rounded-md shadow-sm  focus:outline-none focus:border-gray-200 focus:ring-0 sm:text-md'
                            {...register('duration')}
                        />
                        {errors.duration && errors.duration.message && <div className='el-input-error'>{errors.duration.message}</div>}
                    </div>
                </div>
            </div>
            <div className='pt-8'>
                <button
                    type='submit'
                    className='flex justify-center w-full h-12 px-4 py-3 text-2xl font-medium text-white border border-transparent rounded-2xl shadow-sm bg-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-200'
                    disabled={isSubmitting}
                >
                    Next
                    {isSubmitting}
                </button>
            </div>
            </div>
        </form>
    )
}
FirstStep.propTypes = {
    onSuccess: PropTypes.func.isRequired,
}
export default FirstStep;
