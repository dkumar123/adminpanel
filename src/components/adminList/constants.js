import moment from 'moment';



export const mapRegisterDetails = (details,values) => (
    {
    test_name: details.testName,
    number_of_questions: details.numberOfQuestion,
    duration: details.duration,
    board_id: values.board,
    std_id: values.class,
    subject_id: values.subject,
    topic_ids:values.topic,
    date:moment().format('DD/MM/YYYY')
  }
);


