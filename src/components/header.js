import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Header = ({ goToOurProgram, goToSuccess, olympiad, class_nine }) => {
  const [open, setOpen] = useState(false);
  return (
    <header className='relative'>
      <div className='pt-6 '>
        <nav className='relative flex items-center justify-between px-4 mx-auto max-w-7xl sm:px-6' aria-label='Global'>
          <div className='flex items-center flex-1'>
            <div className='flex items-center justify-between w-full md:w-auto'>
              <a href='#'>
                <img className='w-auto h-8 sm:h-20' src='/images/masterlogopng.png' alt='' />
              </a>
              <div className='flex items-center -mr-2 md:hidden'>
                <button
                  type='button'
                  className='inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-600'
                  aria-expanded='false'
                  onClick={() => setOpen(true)}
                >
                  <span className='sr-only'>Open main menu</span>

                  <svg
                    className='w-6 h-6'
                    xmlns='http://www.w3.org/2000/svg'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'
                    aria-hidden='true'
                  >
                    <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M4 6h16M4 12h16M4 18h16' />
                  </svg>
                </button>
              </div>
            </div>
          </div>
          <div className='hidden md:flex md:items-center md:space-x-6'>
            <a
              href='#'
              onClick={(e) => {
                e.preventDefault();
                goToOurProgram();
              }}
              className='text-base font-medium hover:text-gray-300'
            >
              Our Programs
            </a>

            <a
              href='#'
              onClick={(e) => {
                e.preventDefault();
                goToSuccess();
              }}
              className='text-base font-medium hover:text-gray-300'
            >
              Results
            </a>
            <a
              href='https://blog.7classes.com/'
              className='text-base font-medium hover:text-gray-300'
              target='_blank'
              rel='noreferrer'
            >
              Blog
            </a>
            <a
              href='https://play.google.com/store/apps/details?id=com.instapreps&amp;hl=en_US&amp;gl=US'
              className='text-base font-medium hover:text-gray-300'
              target='_blank'
              rel='noreferrer'
            >
              Download app
            </a>

            {olympiad && (
              <div class='group inline-block'>
                <button class='outline-none focus:outline-none border px-3 py-1 bg-white rounded-sm flex items-center min-w-32'>
                  <span class='pr-1 font-semibold flex-1'>Olympiad</span>
                  <span>
                    <svg
                      class='fill-current h-4 w-4 transform group-hover:-rotate-180
                    transition duration-150 ease-in-out'
                      xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 20 20'
                    >
                      <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                    </svg>
                  </span>
                </button>
                <ul
                  class='bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute 
              transition duration-150 ease-in-out origin-top min-w-32 z-40'
                >
                  <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>
                    <button class='w-full text-left flex items-center outline-none focus:outline-none'>
                      <span class='pr-1 flex-1'>Class 6</span>
                      <span class='mr-auto'>
                        <svg
                          class='fill-current h-4 w-4
                        transition duration-150 ease-in-out'
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 20 20'
                        >
                          <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                        </svg>
                      </span>
                    </button>
                    <ul
                      class='bg-white border rounded-sm absolute top-0 right-0 
              transition duration-150 ease-in-out origin-top-left
              min-w-50
              '
                    >
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Science Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Math Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Scicene + Math Olympiad</li>
                    </ul>
                  </li>
                  <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>
                    <button class='w-full text-left flex items-center outline-none focus:outline-none'>
                      <span class='pr-1 flex-1'>Class 7</span>
                      <span class='mr-auto'>
                        <svg
                          class='fill-current h-4 w-4
                        transition duration-150 ease-in-out'
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 20 20'
                        >
                          <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                        </svg>
                      </span>
                    </button>
                    <ul
                      class='bg-white border rounded-sm absolute top-0 right-0 
              transition duration-150 ease-in-out origin-top-left
              min-w-50
              '
                    >
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Science Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Math Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Scicene + Math Olympiad</li>
                    </ul>
                  </li>
                  <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>
                    <button class='w-full text-left flex items-center outline-none focus:outline-none'>
                      <span class='pr-1 flex-1'>Class 8</span>
                      <span class='mr-auto'>
                        <svg
                          class='fill-current h-4 w-4
                        transition duration-150 ease-in-out'
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 20 20'
                        >
                          <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                        </svg>
                      </span>
                    </button>
                    <ul
                      class='bg-white border rounded-sm absolute top-0 right-0 
              transition duration-150 ease-in-out origin-top-left
              min-w-50
              '
                    >
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Science Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Math Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Scicene + Math Olympiad</li>
                    </ul>
                  </li>
                  <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>
                    <button class='w-full text-left flex items-center outline-none focus:outline-none'>
                      <span class='pr-1 flex-1'>Class 9</span>
                      <span class='mr-auto'>
                        <svg
                          class='fill-current h-4 w-4
                        transition duration-150 ease-in-out'
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 20 20'
                        >
                          <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                        </svg>
                      </span>
                    </button>
                    <ul
                      class='bg-white border rounded-sm absolute top-0 right-0 
              transition duration-150 ease-in-out origin-top-left
              min-w-50
              '
                    >
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Science Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Math Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Scicene + Math Olympiad</li>
                    </ul>
                  </li>
                  <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>
                    <button class='w-full text-left flex items-center outline-none focus:outline-none'>
                      <span class='pr-1 flex-1'>Class 10</span>
                      <span class='mr-auto'>
                        <svg
                          class='fill-current h-4 w-4
                        transition duration-150 ease-in-out'
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 20 20'
                        >
                          <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                        </svg>
                      </span>
                    </button>
                    <ul
                      class='bg-white border rounded-sm absolute top-0 right-0 
              transition duration-150 ease-in-out origin-top-left
              min-w-50
              '
                    >
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Science Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Math Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Scicene + Math Olympiad</li>
                    </ul>
                  </li>
                </ul>
              </div>
            )}
            {class_nine && (
              <div class='group inline-block'>
                <button class='outline-none focus:outline-none border px-3 py-1 bg-white rounded-sm flex items-center min-w-32'>
                  <span class='pr-1 font-semibold flex-1'>Courses</span>
                  <span>
                    <svg
                      class='fill-current h-4 w-4 transform group-hover:-rotate-180
                    transition duration-150 ease-in-out'
                      xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 20 20'
                    >
                      <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                    </svg>
                  </span>
                </button>
                <ul
                  class='bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute 
              transition duration-150 ease-in-out origin-top min-w-32 z-40'
                >
                  <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>
                    <button class='w-full text-left flex items-center outline-none focus:outline-none'>
                      <span class='pr-1 flex-1'>Class 6</span>
                      <span class='mr-auto'>
                        <svg
                          class='fill-current h-4 w-4
                        transition duration-150 ease-in-out'
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 20 20'
                        >
                          <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                        </svg>
                      </span>
                    </button>
                    <ul
                      class='bg-white border rounded-sm absolute top-0 right-0 
              transition duration-150 ease-in-out origin-top-left
              min-w-50'
                    >
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Science mtahs</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Math Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Scicene + Math Olympiad</li>
                    </ul>
                  </li>
                  <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>
                    <button class='w-full text-left flex items-center outline-none focus:outline-none'>
                      <span class='pr-1 flex-1'>Class 7</span>
                      <span class='mr-auto'>
                        <svg
                          class='fill-current h-4 w-4
                        transition duration-150 ease-in-out'
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 20 20'
                        >
                          <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                        </svg>
                      </span>
                    </button>
                    <ul
                      class='bg-white border rounded-sm absolute top-0 right-0 
              transition duration-150 ease-in-out origin-top-left
              min-w-50
              '
                    >
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Science Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Math Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Scicene + Math Olympiad</li>
                    </ul>
                  </li>
                  <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>
                    <button class='w-full text-left flex items-center outline-none focus:outline-none'>
                      <span class='pr-1 flex-1'>Class 8</span>
                      <span class='mr-auto'>
                        <svg
                          class='fill-current h-4 w-4
                        transition duration-150 ease-in-out'
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 20 20'
                        >
                          <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                        </svg>
                      </span>
                    </button>
                    <ul
                      class='bg-white border rounded-sm absolute top-0 right-0 
              transition duration-150 ease-in-out origin-top-left
              min-w-50'
                    >
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Science Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Math Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Scicene + Math Olympiad</li>
                    </ul>
                  </li>
                  <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>
                    <button class='w-full text-left flex items-center outline-none focus:outline-none'>
                      <span class='pr-1 flex-1'>Class 9</span>
                      <span class='mr-auto'>
                        <svg
                          class='fill-current h-4 w-4
                        transition duration-150 ease-in-out'
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 20 20'
                        >
                          <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                        </svg>
                      </span>
                    </button>
                    <ul
                      class='bg-white border rounded-sm absolute top-0 right-0 
              transition duration-150 ease-in-out origin-top-left
              min-w-50
              '
                    >
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Science Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Math Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Scicene + Math Olympiad</li>
                    </ul>
                  </li>
                  <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>
                    <button class='w-full text-left flex items-center outline-none focus:outline-none'>
                      <span class='pr-1 flex-1'>Class 10</span>
                      <span class='mr-auto'>
                        <svg
                          class='fill-current h-4 w-4
                        transition duration-150 ease-in-out'
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 20 20'
                        >
                          <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
                        </svg>
                      </span>
                    </button>
                    <ul
                      class='bg-white border rounded-sm absolute top-0 right-0 
              transition duration-150 ease-in-out origin-top-left
              min-w-50
              '
                    >
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Science Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Math Olympiad</li>
                      <li class='rounded-sm relative px-3 py-1 hover:bg-gray-100'>Scicene + Math Olympiad</li>
                    </ul>
                  </li>
                </ul>
              </div>
            )}
            {olympiad && (
              <a
                href='#'
                onClick={(e) => {
                  e.preventDefault();
                  goToOurProgram();
                }}
                className='text-base font-medium hover:text-gray-300'
              >
                Study Material
              </a>
            )}
            {class_nine && (
              <a
                href='#'
                onClick={(e) => {
                  e.preventDefault();
                  goToOurProgram();
                }}
                className='text-base font-medium hover:text-gray-300'
              >
                Study Material
              </a>
            )}
            <a
              href='https://demo.7classes.com/login'
              className='inline-flex items-center px-4 py-2 text-base font-medium text-white border border-transparent rounded-md shadow-lg bg-green hover:bg-gray-700'
              target='_target'
            >
              Login
            </a>
          </div>
        </nav>
      </div>

      {/* Mobile menus start */}
      <div
        className={`${
          open ? 'block' : 'hidden'
        } z-20 absolute inset-x-0 top-0 pt-2 transition origin-top transform md:hidden`}
      >
        <div className='overflow-hidden  bg-white rounded-lg ring-black ring-opacity-5'>
          <div className='flex items-center justify-between px-4 pt-4'>
            <div>
              <img className='w-auto h-8 sm:h-20' src='/images/masterlogopng.png' alt='' />
            </div>
            <div className='-mr-2'>
              <button
                type='button'
                className='inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-600'
                onClick={() => setOpen(false)}
              >
                <span className='sr-only'>Close menu</span>
                <svg
                  className='w-6 h-6'
                  xmlns='http://www.w3.org/2000/svg'
                  fill='none'
                  viewBox='0 0 24 24'
                  stroke='currentColor'
                  aria-hidden='true'
                >
                  <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M6 18L18 6M6 6l12 12' />
                </svg>
              </button>
            </div>
          </div>
          <div className={`${open ? 'block' : 'hidden'} pt-5 pb-6`}>
            <div className='px-2 flex flex-col items-center  space-y-1'>
              <a href='#' 
              onClick={(e) => {
                e.preventDefault();
                goToOurProgram();
              }}
              className='block px-3 py-2 text-base font-medium text-gray-900 rounded-md hover:bg-gray-50'>
                Our Programs
              </a>
              <a href='#' 
              href='https://blog.7classes.com/'
              className='text-base font-medium hover:text-gray-300'
              target='_blank'
              rel='noreferrer'
              className='block px-3 py-2 text-base font-medium text-gray-900 rounded-md hover:bg-gray-50'>
                Blog
              </a>
              <a href='#'
              onClick={(e) => {
                e.preventDefault();
                goToSuccess();
              }}
              className='block px-3 py-2 text-base font-medium text-gray-900 rounded-md hover:bg-gray-50'>
                Results
              </a>

              <a
                href='https://play.google.com/store/apps/details?id=com.instapreps&amp;hl=en_US&amp;gl=US'
                className='block px-3 py-2 text-base font-medium text-gray-900 rounded-md hover:bg-gray-50'
                target='_blank'
                rel='noreferrer'
              >
                Download app
              </a>
              <a href='#' className='block px-3 py-2 text-base font-medium text-gray-900 rounded-md hover:bg-gray-50'>
                Olympiad
              </a>
              <a href='#' className='block px-3 py-2 text-base font-medium text-gray-900 rounded-md hover:bg-gray-50'>
                Study Material
              </a>
            </div>
            <div className='px-5 mt-6 flex justify-center '>
              <a
                href='https://demo.7classes.com/login'
                className='inline-flex items-center px-4 py-2 text-base font-medium text-white border border-transparent rounded-md shadow-lg bg-green hover:bg-gray-700'
                target='_target'
              >
                Login
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  goToOurProgram: PropTypes.func.isRequired,
  goToSuccess: PropTypes.func.isRequired
};

export default Header;
