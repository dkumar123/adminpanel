export const accordionData = [
  {
    title: 'How long is the SAT?',
    content: `With the essay, the SAT lasts 3 hours and 50 minutes.
              Without the essay, it takes 3 hours. Every SAT test contains the same four sections: Reading, Writing and Language, Math - No Calculator, and Math - Calculator in that order. Some test administrations also include experimental questions, which would extend your total testing time by 20 minutes.
    `
  },
  {
    title: 'How many times can you take the SAT?',
    content:`With the essay, the SAT lasts 3 hours and 50 minutes.
    Without the essay, it takes 3 hours. Every SAT test contains the same four sections: Reading, Writing and Language, Math - No Calculator, and Math - Calculator in that order. Some test administrations also include experimental questions, which would extend your total testing time by 20 minutes.
`
  },
  {
    title: 'What is the difference between ACT and SAT?' ,
    content: `With the essay, the SAT lasts 3 hours and 50 minutes.
    Without the essay, it takes 3 hours. Every SAT test contains the same four sections: Reading, Writing and Language, Math - No Calculator, and Math - Calculator in that order. Some test administrations also include experimental questions, which would extend your total testing time by 20 minutes.
`
  }
];
